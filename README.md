This is an helm package to deploy TocToc with muppy.

TocToc project URL is https://gitlab.com/cmorisse/appserver-toctoc

# Build this package

```bash
# in chart folder

# This will rebuild dependencies
rm -rf charts
rm -rf Chart.lock
helm dependency build .

helm package --dependency-update . --destination=pkg

# refresh
helm dependency update .


# 
helm -n toctoc list
helm -n toctoc delete toctoc

helm -n toctoc --debug --dry-run toctoc . -f values.yaml 
helm -n toctoc install --debug --dry-run toctoc . -f toctoc-4a6fdd06-mpy_values.yaml > helm-debug.txt


```

# To publish the TocToc Chart Helm to Gitlab Registry

Adjust and commit all changes before.

```bash
cd utils
export HELM_CHART_NAME=$(basename $(helm package --dependency-update .. | awk '{print $NF}'))  && echo "chart name=${HELM_CHART_NAME}" 
export GITLAB_REGISTRY_USERNAME=cmorisse
export GITLAB_REGISTRY_TOKEN=$REPO_GIT_TOKEN
export GITLAB_PROJECT_ID=56199255
# To upload the package to gitlab registry
curl --request POST \
    --form "chart=@$HELM_CHART_NAME" \
    --user $GITLAB_REGISTRY_USERNAME:$GITLAB_REGISTRY_TOKEN \
    https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/packages/helm/api/stable/charts
rm $HELM_CHART_NAME
```

# To install the package from Gitlab Package Repository

```bash
helm repo add mpy-metapackage-charts https://gitlab.com/api/v4/projects/56199255/packages/helm/stable
helm repo update mpy-metapackage-charts

helm install one mpy-metapackage-charts/mpy-metapackage
```

# Deployment with Muppy

TocToc aims at protecting K8s Clusters in Public mode.

Al kind of Public Clusters are supported; Public and Private + Public Router.

TocToc must always listen on a public IP and TocToc GUI should be protected with ipWhiteListing.

