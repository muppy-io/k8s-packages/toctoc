# Définition des variables
#CC=gcc
#CFLAGS=-I.
#DEPS = # dépendances, par exemple: header.h
#OBJ = # fichiers objets générés à partir des fichiers source, par exemple: main.o module.o

VALUES_FILE ?= "values.yaml"
NAMESPACE ?= "toctoc"
CHART_NAME ?= "toctoc"
#HELM_CHART_NAME ?= "mpy.metapackage"
GITLAB_REGISTRY_USERNAME="${USER}"
GITLAB_REGISTRY_TOKEN="${REPO_GIT_TOKEN}"
GITLAB_PROJECT_ID=56199226

# Cible par défaut affichant l'aide
help:
	@echo "Available targets:"
	@echo "  build                                - Update dependencies and build package"
	@echo "  publish                              - Deploy package on gitlab"
	@echo "  dbg_install [VALUES_FILE=myfile.yml] - Install package in dry mode"
	@echo "  install [VALUES_FILE=myfile.yml]     - Install package in namespace toctoc"
	@echo "  help                                 - Display this message"
	@echo  "ENV Vars:"
	@echo  "  - VALUES_FILE=values.yaml"
	@echo  "  - NAMESPACE=default"
	@echo  "  - CHART_NAME=meta-test-01"
	@echo  ""

# Cible pour construire le projet
build:
	helm dependency update .
	helm dependency build .
	helm package --dependency-update . --destination=pkg

dbg_install:
	helm -n ${NAMESPACE} install --debug --dry-run ${CHART_NAME} . -f ${VALUES_FILE} > helm-debug.txt

install:
	helm -n ${NAMESPACE} install --create-namespace ${CHART_NAME} . -f ${VALUES_FILE}

delete:
	helm -n ${NAMESPACE} delete ${CHART_NAME}

# Cible pour déployer le package da
publish:
	@echo "Déploiement de l'application..."
	@sudo wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && sudo chmod +x /usr/bin/yq
	@export MVBASE64="$$(base64 -w 0 values-muppy.yaml)" && yq eval '.annotations."muppy-values" = strenv(MVBASE64)' --inplace Chart.yaml
	@echo "GITLAB_REGISTRY_USERNAME=${GITLAB_REGISTRY_USERNAME}"
	@echo "GITLAB_REGISTRY_TOKEN=${GITLAB_REGISTRY_TOKEN}"
	export HELM_CHART_NAME=$$(basename $$(helm package --dependency-update . | awk '{print $$NF}')) && echo $$HELM_CHART_NAME ; \
	curl --request POST --form "chart=@$$HELM_CHART_NAME" --user ${GITLAB_REGISTRY_USERNAME}:${GITLAB_REGISTRY_TOKEN} \
    	https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/packages/helm/api/stable/charts ; \
	rm -f "$$HELM_CHART_NAME"

info:
	@echo ""
	@echo "App. Repo. URL =>  https://gitlab.com/cmorisse/appserver-toctoc"
	@echo "Package URL    =>  https://gitlab.com/muppy-io/k8s-packages/toctoc"
	@echo "Repo URL       =>  https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/packages/helm/stable"
	@echo ""

# Cible pour nettoyer le projet (supprime les fichiers objets et l'exécutable)
clean:
	rm -rf pkg/*
	rm -f Chart.lock

# Empêcher make de confondre les cibles sans dépendances avec des fichiers
.PHONY: help build deploy clean
